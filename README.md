# SpringBootFullStackUserManagement

- [SpringBootFullStackUserManagement](#springbootfullstackusermanagement)
- [Verwendete Technologien](#verwendete-technologien)
- [Verwendete Programme](#verwendete-programme)
- [Projekt erstellen](#projekt-erstellen)
  - [Dependencies](#dependencies)
- [Databank Schema aufbauen](#databank-schema-aufbauen)
  - [Datasource Properties](#datasource-properties)
- [Erstellung der Homepage](#erstellung-der-homepage)
- [Konfigurieren Spring Boot DevTools](#konfigurieren-spring-boot-devtools)
- [Bootstrap](#bootstrap)
- [Code Data Access Layer (Repo Layer)](#code-data-access-layer-repo-layer)
- [Code Unit Tests](#code-unit-tests)
- [Code Users Listing Page](#code-users-listing-page)
- [Add User Function](#add-user-function)
- [Edit/Update/delet User Function](#editupdatedelet-user-function)
    - [Controller Layer](#controller-layer)
    - [Service Layer](#service-layer)
- [Delete User Function](#delete-user-function)
    - [Controller Layer](#controller-layer-1)
    - [Service Layer](#service-layer-1)

# Verwendete Technologien

- Spring Boot Web
- Spring Data JPA & Hibernate
- MySQL
- Thymeleaf
- HTML5 & Bootstrap
- JUnit 5 & AssertJ

# Verwendete Programme

- JDK
- IntelliJ IDEA Ultimate
- MySQL Community Server
- MySQL Workbench

# Projekt erstellen

Neues Projekt mit Spring Initializr

## Dependencies

- Spring Boot DevTools
- Spring Web
- Thymeleaf
- Spring Data JPA
- MySQL Driver

# Databank Schema aufbauen

Rechts Database-Tab → Neue Database erstellen (+)

<img src="BerichtImage/Untitled.png" width=600>

Ubuntu Virtuelle Maschine

Docker mit MySQL und Adminer

root@192.168.227.21

User: root

Passwort: 123

DB: userdata

Rechts neues Schema erstellen usersdb mit utf8mb4_general_ci

## Datasource Properties

application.properties

```java
spring.datasource.url=jdbc:mysql://192.168.227.21:3306/usersdb
spring.datasource.username=root
spring.datasource.password=123
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.show_sql=true
```

# Erstellung der Homepage

MainController.java erstellen

@Controller definiert die Klasse als Controller

@GetMapping um GET Anfragen zu verarbeiten

resources → template → index.html erstellen

# Konfigurieren Spring Boot DevTools

Um nicht bei jeder Änderung die ganze Applikation zu beenden und wieder zu starten konfigurieren wird die DevTools.

<img src="BerichtImage/Untitled%201.png" width=600>
<img src="BerichtImage/Untitled%202.png" width=600>
<img src="BerichtImage/Untitled%203.png" width=600>
<img src="BerichtImage/Untitled%204.png" width=600>

# Bootstrap

Zwei Dependicies in die pom.xml einfügen und laden

```xml
<dependency>
            <groupId>org.webjars</groupId>
            <artifactId>bootstrap</artifactId>
            <version>4.3.1</version>
        </dependency>
        <dependency>
            <groupId>org.webjars</groupId>
            <artifactId>webjars-locator-core</artifactId>
        </dependency>

```

index.html - Bootstrap CSS einbinden

```html
<html lang="en" xml:th="http://www.tyhmeleaf.org">
  <link
    rel="stylesheet"
    type="text/css"
    th:href="@{/webjars/bootstrap/css/bootstrap.min.css}"
  />
</html>
```

Neustarten der Applikation

# Code Data Access Layer (Repo Layer)

Erstellung neues Package user → User.java

In der neuen User Klasse werden jetzt alle JPA Mappings für die Datenbankeinträge erstellt dies vereinfacht die Übertragung der Objekte in die Datenbank.

```java
@Entity
@Table(name = "users")

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
@Column(nullable = false, unique = true, length = 45)
private String example;
```

Tabelle wird automatisch generiert

Kann rechts im Database-Tab überprüft werden

# Code Unit Tests

test → java → com.mycompany.mywebapp → UserRepositoryTest.java erstellt

`@AutoWired` teilt Spring mit, wo es mittels Injection Objekte in andere Klassen einfügen soll

Testmethoden erstellen zB

```java
@Test
    public void testAddNew() {
        User user = new User();
        user.setEmail("clemens@hotmail.com");
        user.setPassword("clemi123456");
        user.setFirstName("Clemens");
        user.setLastName("Ker");

        User savedUser = repo.save(user);

        Assertions.assertThat(savedUser).isNotNull();
        Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
    }
```

Assertions Klasse für Bedinungen die als Tests dienen

# Code Users Listing Page

UserService erstellen

UserController erstellen

Aufbau der HTML-Seite für Manage Users

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.tyhmeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Manage Users</title>
    <link rel="stylesheet" type="text/css" th:href="@{/webjars/bootstrap/css/bootstrap.min.css}">
</head>
<body>
<div class="container-fluid text-center">
    <div><h2>Manage Users</h2></div>

    <div th:if="${message}" class="alert alert-success text-center">
        [[${message}]]
    </div>

    <div class="m-2">
        <a class="h3" th:href="@{/users/new }">Add New User</a>
    </div>
    <div>
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>E-Mail</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Enabled</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <th:block th:each="user : ${listUsers}">
                <tr>
                    <td>[[${user.id}]]</td>
                    <td> [[${user.email}]]</td>
                    <td> [[${user.firstName}]]</td>
                    <td> [[${user.lastName}]]</td>
                    <td> [[${user.enabled} ? yes : no]]</td>
                    <td>
                        <a class="h4 mr-3" th:href="@{'/users/edit/' + ${user.id}}">Edit</a>
                        <a class="h4" th:href="@{'/users/delete/' + ${user.id}}">Delete</a>
                    </td>
                </tr>
            </th:block>

            </tbody>
        </table>
    </div>
</div>
</body>
</html>
```

1:09:30

# Add User Function

UserController → GetMapping Methode erstellt um User Form für neuen Benutzer anlegen anzuzeigen

users_from.html erstellen - Formular erstellen

Eingabefelder mit Validierungen

JavaScript Funktion für Cancel Button

Submit Button - UserController Handler Methode mit PostMapping erstellen (th:action="@{/users/save}" → PostMapping /users/save)

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.tyhmeleaf.org">
  <head>
    <meta charset="UTF-8" />
    <title>Add New User</title>
    <link
      rel="stylesheet"
      type="text/css"
      th:href="@{/webjars/bootstrap/css/bootstrap.min.css}"
    />
  </head>
  <body>
    <div class="container-fluid">
      <div class="text-center"><h1>Add New User</h1></div>

      <form
        th:action="@{/users/save}"
        method="post"
        th:object="${user}"
        style="max-width: 500px; margin: 0 auto;"
      >
        <div class="border border-secondary rounded p-3">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Email:</label>
            <div class="col-sm-8 ">
              <input
                type="email"
                th:field="*{email}"
                class="form-control"
                required
                minlength="8"
                maxlength="45"
              />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 col-form-label">First Name:</label>
            <div class="col-sm-8 ">
              <input
                type="text"
                th:field="*{firstName}"
                class="form-control"
                required
                minlength="2"
                maxlength="45"
              />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Last Name:</label>
            <div class="col-sm-8 ">
              <input
                type="text"
                th:field="*{lastName}"
                class="form-control"
                required
                minlength="2"
                maxlength="45"
              />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Password:</label>
            <div class="col-sm-8 ">
              <input
                type="password"
                th:field="*{password}"
                class="form-control"
                required
                minlength="5"
                maxlength="15"
              />
            </div>
          </div>

          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Enabled:</label>
            <div class="col-sm-8 ">
              <input type="checkbox" th:field="*{enabled}" />
            </div>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary m-2">Save</button>
            <button
              type="button"
              class="btn btn-secondary m-2"
              onclick="cancelForm()"
            >
              Cancel
            </button>
          </div>
        </div>
      </form>
    </div>
    <script type="text/javascript">
      function cancelForm() {
        window.location = "[[@{/users}]]";
      }
    </script>
  </body>
</html>
```

Ausgabe für Benutzer nachdem User erstellt wurde - auf users.html weil man danach auf diese weitergeleitet wird

```html
<div th:if="${message}" class="alert alert-success text-center">
  [[${message}]]
</div>
```

# Edit/Update/delet User Function

Wenn bei der Userliste der Edit Button geklickt wird öffnet es das gleiche Formular (users_form) und der Benutzer kann den User ändern. Um das gleiche Formular verwenden zu können wurden nur die Überschrift mit Tyhmeleaf Attributen geändert.

### Controller Layer

```java
@GetMapping("/users/edit/{id}")
    public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes redirectAttributes) {
        try {
            User user = service.get(id);
            model.addAttribute("user", user);
            model.addAttribute("pageTitle", "Edit User (ID: " + id + ")");
            return "users_form";
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/users";
        }
    }
```

### Service Layer

```java
public User get(Integer id) throws UserNotFoundException {
        Optional<User> result = repo.findById(id);
        if (result.isPresent()) {
            return result.get();
        }
        throw new UserNotFoundException("Could not find any users with ID " + id);
    }
```

# Delete User Function

Gleiche Schritte wie bei Edit nur das der Service Layer eine andere Logik beinhaltet

### Controller Layer

```java
@GetMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
        try {
            service.delete(id);
            redirectAttributes.addFlashAttribute("message", "The user ID " + id + " has been deleted.");
        } catch (UserNotFoundException e) {
            redirectAttributes.addFlashAttribute("message", e.getMessage());
        }
        return "redirect:/users";
    }
```

### Service Layer

```java
public void delete(Integer id) throws UserNotFoundException {
        Long count = repo.countById(id);
        if (count == null || count == 0) {
            throw new UserNotFoundException("Could not find any users with ID " + id);
        }
        repo.deleteById(id);
    }
```
